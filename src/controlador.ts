import { Factura } from "./factura.class";
import { Impresora } from "./impresora.class";
import { imprimir } from "./utilidades";
import { TiposFactura } from "./tipos-factura.enum";


let esPar = function (f: Factura): boolean {
    return f.base % 2 === 0;
}


export function main() {
    let factura1 = new Factura(TiposFactura.Gasto, 357.45, 21, "Limpieza cocina");
    let factura2 = new Factura(TiposFactura.Gasto, 250, 21, "Limpieza baño");
    let factura3 = new Factura(TiposFactura.Ingreso, 4, 0, "Venta wallapop");
    let factura4 = new Factura(TiposFactura.Gasto, 3, 7, "Ferrari");

    let hp1 = new Impresora('HP', 'Deskjet 1000');

    let facturas = [factura1, factura2, factura3, factura4];

    for (let factura of facturas) {
        imprimir(hp1, factura);
    }

    facturas.filter(esPar);

    let gastos = facturas.filter(factura => factura.tipo === TiposFactura.Gasto);
    let ingresos = facturas.filter(factura => factura.tipo === TiposFactura.Ingreso);
    let nGastos = gastos.length;
    let nIngresos = ingresos.length;

    console.log(`Hay ${nGastos} facturas de gastos y ${nIngresos} factura${nIngresos !== 1 ? 's' : ''} de ingresos`);

    let totalGastos = gastos.reduce((acc: number, factura) => acc + factura.base, 0);
    let totalIngresos = ingresos.reduce((acc: number, factura) => acc + factura.base, 0);

    console.log(`Total gastos: ${totalGastos}€`);
    console.log(`Total ingresos: ${totalIngresos}€`);
}