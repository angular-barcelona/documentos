import { Documento } from "./documento.interface";
import { TiposFactura } from "./tipos-factura.enum";

export class Factura implements Documento {
    public numero: number;
    public iva: number;
    public total: number;
    public static ultimoNumero: number = 0;

    constructor(public tipo: TiposFactura, public base: number, public tipoIva: number, public concepto: string) {
        this.incrementaNumeroFactura();
        this.calculaIva();
        this.calculaTotal();
    }

    private incrementaNumeroFactura(): void {
        this.numero = ++Factura.ultimoNumero;
    }

    private calculaIva(): void {
        this.iva = this.base * this.tipoIva / 100;
    }

    private calculaTotal(): void {
        this.total = this.base + this.iva;
    }
}