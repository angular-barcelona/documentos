import { Documento } from "./documento.interface";
import { Factura } from "./factura.class";

export class Impresora {
    constructor(public marca: string, public modelo: string) { }

    public imprimir(documento: Documento) {
        console.log(`Imprimiendo el documento ${documento.numero} por la impresora ${this.marca} ${this.modelo}`);
        if (documento instanceof Factura) {
            console.log(`
            Concepto: ${documento.concepto}
            Base: ${documento.base.toFixed(2)}
            Tipo IVA: ${documento.tipoIva}
            IVA: ${documento.iva.toFixed(2)}
            Total: ${documento.total.toFixed(2)}`);
        }
    }
}