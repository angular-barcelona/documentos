import { Documento } from './documento.interface';
import { Impresora } from './impresora.class';

export function imprimir(impresora: Impresora, documento: Documento) {
    impresora.imprimir(documento);
}